if Person.admins.empty?
  admin = User.create!(:login => 'adminuser', :email => 'admin@example.com',
                       :password => 'admin', :password_confirmation => 'admin',
                       :environment => Environment.default, :activated_at => Time.new)
  admin.activate

  Environment.all.each do |env|
    env.add_admin admin.person
  end
end