Environment.all.each do |env|
  Noosfero::Plugin.available_plugin_names.each do |plugin|
    plugin_name = plugin.to_s + "Plugin"
    unless env.enabled_plugins.include?(plugin_name)
      env.enabled_plugins += [plugin_name]
    end
  end
  env.theme = 'stoa'
  env.save!
end
