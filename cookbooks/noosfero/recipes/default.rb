package 'postgresql'
package 'git-core'
package 'ruby-sinatra'

template '/etc/apt/sources.list' do
  owner 'root'
  group 'root'
  mode 0644
end

execute 'noosfero:sign-key' do
  command "wget -O - http://download.noosfero.org/debian/wheezy/signing-key.asc | sudo apt-key add -"
end

execute 'noosfero:update-packages-list' do
  command "apt-get update"
end

#FIXME: should we remove this after an upgrade to the latest noosfero version?
package 'ruby-rails-3.2' do
  action :upgrade
end

package 'noosfero' do
  action :upgrade
end

package 'ruby-rack-protection' do
  version '1.2.0-1+noosfero1'
end

execute 'noosfero:set:domain' do
  command "sudo -u noosfero RAILS_ENV=production bundle exec rails runner \"Environment.default.domains << Domain.new(:name => '#{node['config']['noosfero']['domain']}', :is_default => true)\""
  cwd '/usr/share/noosfero'
end

package 'noosfero-apache' do
  action :upgrade
end

execute 'apache:disable_default' do
  command 'a2dissite 000-default'
end

service 'apache2' do
  supports reload: true
  action :reload
end

execute 'stoa:theme:update' do
  command 'cd stoa; git pull origin master'
  cwd '/usr/share/noosfero/public/designs/themes'
  only_if { File.exists?('/usr/share/noosfero/public/designs/themes/stoa') }
end

execute 'stoa:theme:install' do
  command 'git clone https://gitlab.com/stoa/stoa-theme.git stoa'
  cwd '/usr/share/noosfero/public/designs/themes'
  not_if { File.exists?('/usr/share/noosfero/public/designs/themes/stoa') }
end

plugins = [
  'custom_forms',
  'people_block',
  'require_auth_to_comment',
  'social_share_privacy',
  'spaminator',
  'statistics',
  'stoa',
  'sub_organizations',
  'tolerance_time',
  'video',
  'vote',
  'work_assignment'
]

execute 'plugins:enable' do
  command '/usr/share/noosfero/script/noosfero-plugins enable ' + plugins.join(' ')
end

service 'noosfero' do
  supports restart: true
  action :restart
end

directory '/tmp/noosfero-scripts' do
  owner  'noosfero'
  group  'noosfero'
  mode   0755
end

#TODO: In earlier Noosfero versions, there is a rake task to enable
#  plugins: "rake noosfero:plugins:enable_all"
#  This file and environment:setup should be replaced by it after
#  further upgrades
cookbook_file '/tmp/noosfero-scripts/env_setup.rb' do
  owner 'noosfero'; group 'noosfero'; mode 0755
  source 'env_setup.rb'
end

cookbook_file '/tmp/noosfero-scripts/add_admin.rb' do
  owner 'noosfero'; group 'noosfero'; mode 0755
  source 'add_admin.rb'
end

execute 'environment:setup:admin' do
  command 'sudo -u noosfero RAILS_ENV=production rails runner /tmp/noosfero-scripts/add_admin.rb'
  cwd '/usr/share/noosfero'
end

execute 'environment:setup' do
  command 'sudo -u noosfero RAILS_ENV=production rails runner /tmp/noosfero-scripts/env_setup.rb'
  cwd '/usr/share/noosfero'
end

#FIXME: This daemon needs a patch to fix the path to /etc/default/noosfero
execute 'sota:daemon' do
  command 'sudo cp -n /usr/share/noosfero/plugins/stoa/script/stoa-auth-server /etc/init.d/ && sudo service stoa-auth-server restart'
end
