-- MySQL dump 10.13  Distrib 5.5.43, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: usp
-- ------------------------------------------------------
-- Server version	5.5.43-0ubuntu0.14.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alunoturma_gr`
--

DROP TABLE IF EXISTS `alunoturma_gr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alunoturma_gr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coddis` char(7) NOT NULL,
  `codtur` char(10) NOT NULL,
  `codpes` int(10) unsigned NOT NULL,
  `verdis` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92660700 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alunoturma_moodle`
--

DROP TABLE IF EXISTS `alunoturma_moodle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alunoturma_moodle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codmoodle` varchar(22) NOT NULL,
  `codpes` int(10) unsigned NOT NULL,
  `tipo` enum('GR','POS') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_alunoturma_moodle_codpes` (`codpes`),
  KEY `idx_alunoturma_moodle_codmoodle` (`codmoodle`),
  KEY `idx_codpes` (`codpes`),
  KEY `idx_codmoodle` (`codmoodle`)
) ENGINE=InnoDB AUTO_INCREMENT=94537743 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alunoturma_pos`
--

DROP TABLE IF EXISTS `alunoturma_pos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alunoturma_pos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sgldis` char(7) NOT NULL,
  `numseqdis` tinyint(3) unsigned NOT NULL,
  `numofe` smallint(5) unsigned DEFAULT NULL,
  `codpes` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6194149 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `disciplinas_gr`
--

DROP TABLE IF EXISTS `disciplinas_gr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disciplinas_gr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coddis` char(7) NOT NULL,
  `verdis` tinyint(3) unsigned NOT NULL,
  `nomdis` varchar(240) NOT NULL,
  `creaul` tinyint(3) unsigned DEFAULT NULL,
  `cretrb` tinyint(3) unsigned DEFAULT NULL,
  `dtaatvdis` datetime DEFAULT NULL,
  `dtadtvdis` datetime DEFAULT NULL,
  `durdis` tinyint(3) unsigned DEFAULT NULL,
  `objdis` text,
  `numvagdis` smallint(6) DEFAULT NULL,
  `numvagdiscpl` tinyint(3) unsigned DEFAULT NULL,
  `pgmdis` text,
  `pgmrsudis` text,
  `tipdis` char(1) DEFAULT NULL,
  `cgahoreto` smallint(6) DEFAULT NULL,
  `staarecln` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2023432 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `disciplinas_pos`
--

DROP TABLE IF EXISTS `disciplinas_pos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disciplinas_pos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sgldis` char(7) NOT NULL,
  `numseqdis` tinyint(3) unsigned NOT NULL,
  `dtaprpdis` datetime NOT NULL,
  `nomdis` varchar(240) NOT NULL,
  `tipcurdis` char(3) NOT NULL,
  `durdis` tinyint(3) unsigned NOT NULL,
  `cgahorteodis` smallint(6) NOT NULL,
  `cgahoresddis` smallint(6) NOT NULL,
  `cgahordis` int(11) NOT NULL,
  `numcretotdis` int(11) NOT NULL,
  `dtaatvdis` datetime DEFAULT NULL,
  `dtadtvdis` datetime DEFAULT NULL,
  `objdis` text,
  `jusdis` text,
  `tipavldis` varchar(160) DEFAULT NULL,
  `ctudis` text,
  `ctubbgdis` text,
  `obsdis` text,
  `codare` int(11) DEFAULT NULL,
  `dtaaprcog` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2185639 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `emailpessoa`
--

DROP TABLE IF EXISTS `emailpessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailpessoa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codpes` int(10) unsigned NOT NULL COMMENT 'Número USP',
  `codema` varchar(80) DEFAULT NULL COMMENT 'Email',
  `numseqema` tinyint(4) DEFAULT NULL COMMENT 'Número sequencial do email da pessoa',
  `stamtr` enum('S','N') DEFAULT NULL COMMENT 'Para mandar mensagens sobre a matrícula',
  `staatnsen` enum('S','N') DEFAULT NULL COMMENT 'Para mandar senha (inútil, ninguém preencheu)',
  PRIMARY KEY (`id`),
  KEY `idx_emailpessoa_codema` (`codema`),
  KEY `idx_emailpessoa_codpes` (`codpes`),
  KEY `idx_emailpessoa_codpes_codema` (`codpes`,`codema`)
) ENGINE=InnoDB AUTO_INCREMENT=105842137 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pessoa`
--

DROP TABLE IF EXISTS `pessoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pessoa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codpes` int(10) unsigned NOT NULL COMMENT 'Número USP',
  `nompes` varchar(128) DEFAULT NULL COMMENT 'Nome completo',
  `numcpf` binary(32) DEFAULT NULL COMMENT 'Hash do CPF',
  `sexpes` enum('M','F') DEFAULT NULL COMMENT 'Sexo',
  `dtanas` date DEFAULT NULL COMMENT 'Data de nascimento',
  `tipdocidf` char(6) DEFAULT NULL,
  `numdocidf` char(17) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codpes_UNIQUE` (`codpes`),
  KEY `idx_pessoa_codpes` (`codpes`)
) ENGINE=InnoDB AUTO_INCREMENT=160474129 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `prefixodiscip`
--

DROP TABLE IF EXISTS `prefixodiscip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prefixodiscip` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sglfusclgund` varchar(40) NOT NULL,
  `pfxdisval` char(3) NOT NULL,
  `dscpfxdis` varchar(100) DEFAULT NULL,
  `nomclgund` varchar(180) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=285819 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sync_times`
--

DROP TABLE IF EXISTS `sync_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sync_times` (
  `name` varchar(30) NOT NULL,
  `sync` date DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `turmas_gr`
--

DROP TABLE IF EXISTS `turmas_gr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turmas_gr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coddis` char(7) NOT NULL,
  `verdis` tinyint(3) unsigned NOT NULL,
  `codtur` char(10) NOT NULL,
  `codpes` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2817037 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `turmas_moodle`
--

DROP TABLE IF EXISTS `turmas_moodle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turmas_moodle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codmoodle` varchar(22) NOT NULL,
  `codpes` int(10) unsigned NOT NULL,
  `tipo` enum('GR','POS') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4723702 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `turmas_pos`
--

DROP TABLE IF EXISTS `turmas_pos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turmas_pos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sgldis` char(7) NOT NULL,
  `numseqdis` tinyint(3) unsigned NOT NULL,
  `numofe` smallint(5) unsigned DEFAULT NULL,
  `codpes` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=908597 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unidades`
--

DROP TABLE IF EXISTS `unidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codfusclgund` smallint(5) unsigned NOT NULL,
  `codoriclgund` smallint(5) unsigned NOT NULL,
  `nomclgund` varchar(180) DEFAULT NULL,
  `sglfusclgund` varchar(40) DEFAULT NULL,
  `clsgrpanr` tinyint(4) DEFAULT NULL,
  `grpsglanr` varchar(50) DEFAULT NULL,
  `nomloc` varchar(30) DEFAULT NULL,
  `urlsie` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codfusclgund` (`codfusclgund`)
) ENGINE=InnoDB AUTO_INCREMENT=29944 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vinculopessoausp`
--

DROP TABLE IF EXISTS `vinculopessoausp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vinculopessoausp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codpes` int(10) unsigned NOT NULL COMMENT 'Número USP',
  `tipvin` varchar(25) DEFAULT NULL COMMENT 'tipo de vínculo com a usp: alunogr, alunopos, docente, servidor, estagiário etc. Uma pessoa pode ter vários vínculos',
  `numseqpes` tinyint(4) DEFAULT NULL,
  `dtainivin` date DEFAULT NULL,
  `dtafimvin` date DEFAULT NULL,
  `sitatl` char(1) DEFAULT NULL COMMENT 'situação do vínculo (Ativo, aPosentado, Desligado)',
  `dtainisitatl` date DEFAULT NULL,
  `codfusclgund` smallint(6) DEFAULT NULL COMMENT 'código da unidade',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42024851 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-01 16:40:31
