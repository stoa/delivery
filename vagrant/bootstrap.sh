#!/bin/bash

set -ex

# Install dependencies
apt-get update
apt-get -y install curl python parted

### Create var as a separated disk
# Backup

mkdir ~/bkp
cp -rp /var/* ~/bkp

# Creating...
parted /dev/sdb mklabel msdos
parted /dev/sdb mkpart primary 512 100%
mkfs.ext4 /dev/sdb1
echo `blkid /dev/sdb1 | awk '{print$2}' | sed -e 's/"//g'` /var   ext4   rw,relatime,data=ordered   0   2 >> /etc/fstab
mount /var

# Restore bkp
cp -rp ~/bkp/* /var/
rm -rf ~/bkp
